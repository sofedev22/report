/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nippon.albumproject.service;

import com.nippon.albumproject.dao.SaleDao;
import com.nippon.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author domem
 */
public class ReportService {

    public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getReportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
}
